import Echo from "laravel-echo"

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: 'wss://ws.dev.doqa.dev-ittest.ru',
    transports: ['websocket'],
    auth: {
        headers: {
            // todo: replace with proper authorization token
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLmRldi5kb3FhLmRldi1pdHRlc3QucnVcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNjE1Mjg0NTA0LCJleHAiOjE2MTUzMjA1MDQsIm5iZiI6MTYxNTI4NDUwNCwianRpIjoiWmszaDFibUQ2Q1ZZOWhSQSIsInN1YiI6MSwicHJ2IjoiZDZmMmVkNDliYTlmMjY4ODE3ZDIwM2Q4NmIzOGI1YzdmZjhlNWY0ZSJ9.FoYxmFvFDSDVNBw2c01FwIJtUw2R3AAzWb9Z2X9by2o'
        }
    }
});

// Private channel
window.Echo.private('spaces.1')
    .listen('.case-updated', (e) => {
        console.log(e);
    });

// Public channel
window.Echo.channel('hello')
    .listen('.test', (e) => {
        console.log(e);
    });
